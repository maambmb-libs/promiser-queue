class Queue {

    constructor() {
        this.borrow = [];
        this.excess = [];
    }

    dequeue() {

        return new Promise( function( resolve ) {

            if( this.excess.length > 0 ) {
                resolve(this.excess.shift() );
                return;
            }

            this.borrow.push( ( data ) => { resolve( data ); } );

        }.bind(this));

    }

    enqueue( data ) {

        if( this.borrow.length > 0 ) {
            this.borrow.shift()( data );
            return;
        }

        this.excess.push( data );
    }

    get length() {
        return this.excess.length;
    }

}

module.exports = Queue;

